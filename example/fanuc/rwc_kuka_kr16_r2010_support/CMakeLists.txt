cmake_minimum_required(VERSION 2.8.3)
project(rwc_kuka_kr16_r2010_support)

include($ENV{CATKIN_WS}/src/Macros.cmake)

find_package(catkin REQUIRED)

catkin_package()

rwc_install_share(meshes urdf)

rwc_install_www(meshes)
