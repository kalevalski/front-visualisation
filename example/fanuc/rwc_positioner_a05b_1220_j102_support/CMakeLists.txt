cmake_minimum_required(VERSION 2.8.3)
project(rwc_positioner_a05b_1220_j102_support)

include($ENV{CATKIN_WS}/src/Macros.cmake)

find_package(catkin REQUIRED)

catkin_package()

rwc_install_share(meshes urdf)

rwc_install_www(meshes)
