#!/bin/bash

# Simplify Mesh command
scmd=${CATKIN_WS}/install/lib/rwc_tools/simplify_mesh
# STL File (Mesh) Header Length
mhl=80
# Fixed STL File Header
fhd=$(head -c ${mhl} < /dev/zero | tr '\0' ' ')

for f in $(ls -1 meshes/*.STL | grep -v _simple)
do
  fixed_mesh=/tmp/$(basename ${f%.*})_fixed.${f##*.}
  #sed '1s/^solid/abagy/' ${f} > ${fixed_mesh}
  sed "1s/^.\{${mhl}\}/${fhd}/" ${f} > ${fixed_mesh}
  $scmd --input-mesh ${fixed_mesh} --output-mesh ${f%.*}_simple.${f##*.} --edges-threshold 2000
  unlink ${fixed_mesh}
done
