import json

path_json = "c:/xampp/htdocs/urdf-loaders-master/javascript/example/fanuc.json"

with open(path_json) as f:
    contents = json.load(f)
animData_fanuc = []
for dict in contents:
    list = []
    list.append(dict["robot_state"]["joint_state"]["name"])
    list.append(dict["robot_state"]["joint_state"]["position"])
    animData_fanuc.append(list)

print(animData_fanuc)

with open("c:/xampp/htdocs/urdf-loaders-master/javascript/example/animData_fanuc.json", 'w') as f:
    json.dump(animData_fanuc, f)